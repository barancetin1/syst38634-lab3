package passwordtest;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test 
	public void testCheckPasswordLength() {
		assertTrue("invalid pw length", PasswordValidator.checkPasswordLength("thisisavalidpassword"));
	}
	
	@Test 
	public void testCheckPasswordLengthSpaceException() {
		assertFalse("invalid pw length", PasswordValidator.checkPasswordLength("        "));
	}

	@Test
	public void testCheckPasswordLengthBoundIn() {
		assertTrue("Out of boundary", PasswordValidator.checkPasswordLength("12345678"));
	}
	
	@Test
	public void testCheckPasswordLengthBoundOut() {
		assertFalse("Out of boundary", PasswordValidator.checkPasswordLength("1234567"));
	}
	
	@Test
	public void testHasTwoDigitsMinimum() {
		assertTrue("invalid digit amount", PasswordValidator.hasTwoDigitsMinimum("thisisainvalidpassword22"));
	}
	
	@Test
	public void testHasTwoDigitsMinimumException() {
		assertFalse("invalid string out of bounds", PasswordValidator.hasTwoDigitsMinimum("tHisstringwillhavenodigits"));
	}
	
	@Test
	public void testHasTwoDigitsMinimumBoundIn() {
		assertTrue("Out of boundary", PasswordValidator.hasTwoDigitsMinimum("12"));
	}
	
	@Test
	public void testHasTwoDigitsMinimumBoundOut() {
		assertFalse("Out of boundary", PasswordValidator.hasTwoDigitsMinimum("1"));
	}
	
	

}
