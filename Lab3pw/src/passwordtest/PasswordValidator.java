package passwordtest;

public class PasswordValidator extends Exception {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String tester = "thisstringh as12digits";	
		System.out.println(checkPasswordLength(tester));

	}
	
	
	public static boolean checkPasswordLength(String pw) {	
		int len = pw.length();
		for(int i = 0; i < len; i++) {
			if(Character.isSpaceChar(pw.charAt(i))) {
				return false;
			}
		}
		return pw.length() >= 8;
	}

	
	public static boolean hasTwoDigitsMinimum(String pw) {
		int digitCount = 0;
		int len = pw.length();
		for(int i = 0; i < len; i++) {
			if(Character.isDigit(pw.charAt(i))) {
				digitCount++;
			}
		}
		if(digitCount >= 2) {
			return true;
		}
		return false;		
	}
	
}
